#pragma once
#include "iostream"
using namespace std;

template <class T> class Pole
{
	T *p;
	int s;
	int last;

public:
	Pole(int n) {
		p = new T[this->s = n];
		for (int i = 0; i < s; i++) p[i] = i;
		last = -1;
	}
	~Pole() {
		delete[] p;
	}
	void add(T prvek) {
		if ((last + 1) == s) throw new exception("The array is full");
		p[++last] = prvek;
	}
	int getSize()
	{
		return s;
	}
	int getLast()
	{
		return last;
	}
	T& operator[](const int i)
	{
		if (i > last) throw new exception("out of range");
		return p[i];
	}
	T max()
	{
		T max;
		if (last < 0) throw new exception("array is empty");
		max = p[0];
		for (int i = 1; i <= last; i++)
		{
			if (p[i] > max)
			{
				max = p[i];
			}
		}
		return max;
	}
	T min()
	{
		T min;
		if (last < 0) throw new exception("array is empty");
		min = p[0];
		for (int i = 1; i <= last; i++)
		{
			if (p[i] < min)
			{
				min = p[i];
			}
		}
		return min;
	}
	template<class P> friend ostream& operator<<(ostream &out, Pole<P> &prvek);
	template<class P> friend ofstream& operator<<(ofstream &out, Pole<P> &prvek);
	template<class P> friend ifstream& operator >> (ifstream &in, Pole<P> &prvek);
};
template<class P>ostream& operator<<(ostream &out, Pole<P> &prvek)
{
	for (int i = 0; i < prvek.last; i++) out << prvek[i] << endl;
	return out;
}
template<class P>ofstream& operator<<(ofstream &out, Pole<P> &prvek)
{
	for (int i = 0; i < prvek.last; i++) out << prvek[i] << endl;
	return out;
}
template<class P> ifstream& operator>>(ifstream &in, Pole<P> &prvek)
{
	P tmp;
	string line;
	while (!in.eof())
	{
		getline(in, line);
		line >> tmp;
		prvek.add(tmp);
		
	}
	return in;
}
