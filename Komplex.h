#pragma once
#include "math.h"
#include "iostream"
#include <string>
#include <fstream>
using namespace std;
template<class T> class Komplex{
T real, imag;

public:
	Komplex()
	{
		real = 0;
		imag = 0;
	}
	~Komplex () {}
	Komplex (T _real, T _imag) {
		real = _real;
		imag = _imag;
	}
	Komplex(T _real) {
		real = _real;
		imag = 0;
	}
	
	
	Komplex operator-() {
		return Komplex<T>(-real, -imag);
	}
	Komplex operator~() {
		return Komplex<T>(real, -imag);
	}
	Komplex& operator=(const Komplex<T> &op) {
		real = op.real;
		imag = op.imag;
		return *this;
	}

	T& operator[](int i) {
		if (i == 0) {
			return real;
		}
		if (i == 1) {
			return imag;
		}
		throw ("index out of range");
	}
	T getReal(void) const { return real; }
	T getImag(void) const { return imag; }
	double getModul(void) const { return sqrt(real*real + imag*imag); }
	double getPhase(void) const { return atan2(imag, real); }

	inline friend Komplex operator+(const Komplex &op1, const Komplex &op2) {
		return Komplex(op1.real + op2.real, op1.imag + op2.imag);
	}
	inline friend Komplex operator-(const Komplex &op1, const Komplex &op2)
	{
		return Komplex(op1.real - op2.real, op1.imag - op2.imag);
	}
	inline friend Komplex operator*(const Komplex &op1, const Komplex &op2)
	{
		return Komplex((op1.real*op2.real - op1.imag*op2.imag), (op1.real*op2.imag + op1.imag*op2.real));
	}
	inline friend Komplex operator/(const Komplex &op1, const Komplex &op2)
	{
		if (op2.getImag() == 0 && op2.getReal() == 0)
		{
			throw new exception("cannot divide by zero");
		}
		double div = (op2.real*op2.real) + (op2.imag*op2.imag);
		double real = 0, imag = 0;
		real = ((op1.real*op2.real) + (op1.imag*op2.imag));
		real /= div;
		imag = ((op1.imag*op2.real) - (op1.real*op2.imag));
		imag /= div;
		return Komplex(real, imag);
	}

	inline friend Komplex& operator+=(Komplex &op1, const Komplex &op2)
	{
		op1 = (op1 + op2);
		return op1;
	}
	/*
	inline friend void operator+=(Komplex &op1, const Komplex &op2)
	{
		op1 = (op1 + op2);
	}*/
	friend Komplex& operator-=( Komplex &op1, const Komplex &op2)
	{
		op1 = (op1 - op2);
		return *(new Komplex(op1.getReal(), op1.getImag()));
	}
	friend Komplex& operator*=(Komplex &op1, const Komplex &op2)
	{
		op1 = (op1 * op2);
		return *(new Komplex(op1.getReal(), op1.getImag()));
	}
	friend Komplex& operator/=(Komplex &op1, const Komplex &op2)
	{
		op1 = (op1 / op2);
		return *(new Komplex(op1.getReal(), op1.getImag()));
	}

	inline friend bool operator==(const Komplex &op1, const Komplex &op2)
	{
		if (op1.real == op2.real && op1.imag == op2.imag) {
			return true;
		}
		return false;
	}
	inline friend bool operator!=(const Komplex &op1, const Komplex &op2)
	{
		if (op1.real == op2.real && op1.imag == op2.imag) {
			return false;
		}
		return true;
	}
	inline friend bool operator<( Komplex &op1, Komplex &op2)
	{
		if (op1.getModul()>op2.getModul())
		{
			return false;
		}
		return true;
	}
	inline friend bool operator>( Komplex &op1, Komplex &op2)
	{
		if (op1.getModul()>op2.getModul()) {
			return true;
		}
		return false;
	}

	
	inline friend ofstream& operator<<(ofstream &out, Komplex &komplex)
	{
		out << noshowpos << komplex.getReal() << showpos << komplex.getImag() << "i" << noshowpos;
		//out << " modul: " << komplex.getModul() << " phase: " << komplex.getPhase();
		return 	out;
	}
	inline friend ifstream& operator>>( ifstream& _in, Komplex &komplex)
	{	
		
		char* str = new char[15];
		string inputStr;
		double real, imag;
		int i=0,j = 0;
		
		getline(_in, inputStr);
		while (inputStr[i] != '\0')
		{
			if (inputStr[i] == ';') break;
			i++;
		}
		
		if (inputStr.length() == i + 1)
		{
			real = atoi(inputStr.c_str());
		}else
		{
			while (j < i) {
				str[j] = inputStr[j];
				j++;
			}
			str[j + 1] = '\0';
			real = atoi(str);
			
			j = ++i;
			i = 0;
			while (j < inputStr.length()) {
				str[i] = inputStr[j];
				j++;
				i++;
			}
			str[j + 1] = '\0';
			imag = atoi(str);
		}
		delete[] str;
		komplex = *(new Komplex(real, imag));
		return _in;
	}
	inline friend string& operator >> (string& _in, Komplex &komplex)
	{

		char* str = new char[15];
		double real, imag;
		int i = 0, j = 0;

		while (_in[i] != '\0')
		{
			if (_in[i] == ';') break;
			i++;
		}

		if (_in.length() == i + 1)
		{
			real = atof(_in.c_str());
		}
		else
		{
			while (j < i) {
				str[j] = _in[j];
				j++;
			}
			str[j + 1] = '\0';
			real = atof(str);

			j = ++i;
			i = 0;
			while (j < _in.length()) {
				str[i] = _in[j];
				j++;
				i++;
			}
			str[j + 1] = '\0';
			imag = atof(str);
		}
		delete[] str;
		komplex = *(new Komplex(real, imag));
		return _in;
	}
	inline friend ostream& operator<<(ostream &out, Komplex &komplex)
	{
		out << noshowpos << komplex.getReal() << showpos << komplex.getImag() << "i" << noshowpos;
		//out << " modul: " << komplex.getModul() << " phase: " << komplex.getPhase();
		return 	out;
	}
	inline friend istream& operator >> (istream& _in, Komplex &komplex)
	{
		//char* in;
		char* str = new char[15];
		char* inputStr = new char[30];
		_in >> inputStr;
		double real, imag;
		int i = 0, j = 0;
		while (inputStr[i] != '\0')
		{
			if (inputStr[i] == ';') break;
			i++;
		}

		if (strlen(inputStr) == i + 1)
		{
			real = atoi(inputStr);
		}
		else
		{
			while (j < i) {
				str[j] = inputStr[j];
				j++;
			}
			str[j + 1] = '\0';
			real = atoi(str);

			j = ++i;
			i = 0;
			while (j < strlen(inputStr)) {
				str[i] = inputStr[j];
				j++;
				i++;
			}
			str[j + 1] = '\0';
			imag = atoi(str);
		}
		delete[] str;
		komplex = *(new Komplex(real, imag));
		return _in;
	}
};

