// C++Komplex.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"
#include "stdlib.h"
#include "Pole.h"
#include "Komplex.h"
using namespace std;
typedef Komplex<double> cpx;

template<class T>void bubbleSort(Pole<T> &array, int size) {

	for (int i = 0; i < size - 1; i++) {

		for (int j = 0; j < size - i - 1; j++) {

			if (array[j + 1] < array[j]) {

				T tmp = array[j + 1];

				array[j + 1] = array[j];

				array[j] = tmp;

			}

		}

	}

}



int main()
{
	Pole<cpx> pole(20);
	ifstream file;
	ofstream ofile;

	
	try {
		file.open("prvky.txt");
		file >> pole;
	}
	catch (const exception e)
	{
		cout << "error ocured: " << e.what() << endl;
	}
	file.close();

	ofile.open("Statistika.txt");
	cout << "*****Complex*****" << endl;
	ofile << "*****Complex*****" << endl;

	cout << "array" << endl;
	ofile << "array" << endl;
	cout << pole << endl;
	ofile << pole << endl;
	
	bubbleSort(pole,pole.getLast());
	
	cout << "sorted array" << endl;
	ofile << "sorted array" << endl;
	cout << pole << endl;
	ofile << pole << endl;
	
	ofile<< "max: " << pole.max() <<endl;
	ofile<< "min: " << pole.min() << endl;
	cout << "max: " << pole.max() << endl;
	cout << "min: " << pole.min() << endl;
	ofile.close();

	Komplex<double> A(2,4), B(5,-4), C(2,4), D(4.3,3), E(6.2,4);

	cout <<"A= "<< A <<endl<<"B= "<< B << endl;
	cout <<"A+B: "<< A + B << endl;
	cout <<"A-B: " << A - B << endl;
	cout << "A*B:" << A * B << endl;
	cout << "A/B:" << A / B << endl;
	
	cout << "A==C:" << (A == C) << endl;
	cout << "A!=C:" << (A != C) << endl;
	cout << "A<B:" << (A < B) << endl;

	cout << "vlozte komplex cislo ve tvaru real ; imag" << endl;
	//cin >> A;
	cout << endl << A << endl;
	cout << A << endl;
	cout << A + 3 << endl;
	cout << A + B[0] << endl;
	cout << 3 + A << endl;
	system("pause");
    return 0;
}

