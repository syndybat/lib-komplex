#pragma once
#include "math.h"
#include "iostream"
#include <string>
using namespace std;
template<class T> class Komplex{
T real, imag;

public:
	Komplex()
	{
		real = 0;
		imag = 0;
	}
	~Komplex () {}
	Komplex (T _real, T _imag) {
		real = _real;
		imag = _imag;
	}
	Komplex(double _real) {
		real = _real;
		imag = 0;
	}
	Komplex(short _real) {
		real = _real;
		imag = 0;
	}
	Komplex(long _real) {
		real = _real;
		imag = 0;
	}
	Komplex(int _real) {
		real = _real;
		imag = 0;
	}
	Komplex operator-() {
		return Komplex<T>(-real, imag);
	}
	Komplex operator~() {
		return Komplex<T>(real, -imag);
	}
	Komplex& operator=(const Komplex<T> &op) {
		real = op.real;
		imag = op.imag;
		return *this;
	}

	T& operator[](int i) {
		if (i == 0) {
			return real;
		}
		if (i == 1) {
			return imag;
		}
		throw ("index out of range");
	}
	T getReal() { return real; }
	T getImag() { return imag; }
	T getModul() { return sqrt(real*real + imag*imag); }
	T getPhase() { atan2(imag, real); }

	inline friend Komplex operator+(const Komplex &op1, const Komplex &op2) {
		return Komplex(op1.real + op2.real, op1.imag + op2.imag);
	}
	inline friend Komplex operator-(const Komplex &op1, const Komplex &op2)
	{
		return Komplex(op1.real - op2.real, op1.imag - op2.imag);
	}
	inline friend Komplex operator*(const Komplex &op1, const Komplex &op2)
	{
		return Komplex((op1.real*op2.real - op1.imag*op2.imag), (op1.real*op2.imag + op1.imag*op2.real));
	}
	inline friend Komplex operator/(const Komplex &op1, const Komplex &op2)
	{
		double div = (op2.real*op2.real) + (op2.imag*op2.imag);
		Komplex<P> tmp(0, 0);
		tmp.real = ((op1.real*op2.real) + (op1.imag*op2.imag));
		tmp.real /= div;
		tmp.imag = ((op1.imag*op2.real) - (op1.real*op2.imag));
		tmp.imag /= div;
		return tmp;
	}

	inline friend Komplex& operator+=(const Komplex &op1, const Komplex &op2)
	{
		op1.real += op2.real;
		op1.imag += op2.imag;
		return op1;
	}
	friend Komplex& operator-=(const Komplex &op1, const Komplex &op2)
	{
		op1.real -= op2.real;
		op1.imag -= op2.imag;
		return op1;
	}
	friend Komplex& operator*=(const Komplex &op1, const Komplex &op2)
	{
		double real, imag;
		real = op1.real*op2.real - op1.imag*op2.imag;
		imag = op1.real*op2.imag + op1.imag*op2.real;
		op1.real = real;
		op1.imag = imag;
		return op1;
	}
	friend Komplex& operator/=(const Komplex &op1, const Komplex &op2)
	{
		double div = (op2.real*op2.real) + (op2.imag*op2.imag);
		double real, imag;
		real = ((op1.real*op2.real) + (op1.imag*op2.imag));
		real /= div;
		imag = ((op1.imag*op2.real) - (op1.real*op2.imag));
		imag /= div;
		op1.real = real;
		op1.imag = imag;
		return op1;
	}

	inline friend bool operator==(const Komplex &op1, const Komplex &op2)
	{
		if (op1.real == op2.real && op1.imag == op2.imag) {
			return true;
		}
		return false;
	}
	inline friend bool operator!=(const Komplex &op1, const Komplex &op2)
	{
		if (op1.real == op2.real && op1.imag == op2.imag) {
			return false;
		}
		return true;
	}

	inline friend ostream& operator<<(const ostream &out, const Komplex &komplex)
	{
		return 	out << noshowpos << komplex.getReal() << showpos << komplex.getImag() << "i" << showpos;
	}
	inline friend Komplex operator>>(const ostream &_in, const Komplex &komplex)
	{	
		//char* in;
		char* str = new char[15];
		double real, imag;
		int i,j = 0;
		while (in[i] != '\0')
		{
			if (in[i] == ';') break;
			i++;
		}
		
		if (strlen(in) == i + 1)
		{
			real = atoi(in);
		}else
		{
			while (j < i) {
				str[j] = in[j];
				j++;
			}
			str[j + 1] = '\0';
			real = atoi(str);
			
			j = ++i;
			while (j < strlen(in)) {
				str[j] = in[i+j];
				j++;
			}
			str[j + 1] = '\0';
			imag = atoi(str);
		}
		delete[] str;
		return Komplex(real, imag);
	}
};

